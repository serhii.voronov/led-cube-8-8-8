
<h1 align="center">Welcome to my project LED cube 8*8*8 on Arduino  👋</h1>

## ‼️ PLEASE NOTE ‼️
Please note that despite the upload date of the project, the project itself is a school assignment in 2018. Therefore, the implementation of this project does not correspond to my current ideas about the development of hardware. Nevertheless, a lot of effort was invested in this project, so I think it's right to leave it here.

## ✨ Description

This is my school project from 2019. The most difficult issue in this project was the decision how to connect 512 diodes and at the same time use a reasonable amount of wire. The solution was use matric-soldering method. Each diode has 2 pins – anode and cathode, so in each layer cathode of all diodes is connected. The same for the columns, all column anodes of all diodes are connected together. I used 8*8 wires for all columns and 8 wires for each layer -> 72 totally. Second issue was power suppling. Because of big amount of diodes, electrical consumption could be really high. So, I decided to use dynamic-lightning algorithm, results were pretty good, consumption decreased ~10 times.
<p align="right">
  <img src="photos/photo.png?raw=true" height="400" title="hover text">
</p>

Power consumption in full mode without dynamical lightning: 512 * 30mA = 15.36 A

Power consumption in full mode with dynamical lightning: 1.6 A 

## 💡 Dynamic lighting 

For dynamic lightning I have function renderCube() in which I have a 50 microsecond delay. I use this function in each iteration of loop(). On each call of renderCube() function program change layer of lightning. So, in fact programmatically there is no way for two diodes from different layers to be lit in the same moment.

<p align="right">
  <img src="photos/leds.png?raw=true" height="400" title="hover text">
</p>

### 📌  Project description 
Due to the minimal time,  the  Arduino Nano was used as the central controller. This controller switches nine shift registers to control all layers and columns separately. Analog port A3 is used for the control capacity button. In this case, the TTP223 module was used for invisible control. And analog pins A4 and A5 for communication with an accelerometer. For lightning one LED, it is necessary to open the transistor (one of the layers will be connected, then open the register pin on some columns. Furthermore, in this way, it is possible to control all LEDs dynamically. Here is the electrical scheme of the LED cube: https://easyeda.com/voronser/led-cube

In the cube, there are two modes. The first mode could keep the led-lightning surface parallel to the Earth's surface in any cube position based on accelerometer and gyroscope data, another one is a simulation of the rain via LED, and the last one in normal waves. 
<p align="left">
  <img src="photos/desk.png?raw=true" height="400" title="hover text">
</p>

## 👤  Author

 **Voronov Serhii**

- LinkedIn: [@serhii-voronov](https://www.linkedin.com/in/serhii-voronov/)
- Github: [@serhii.voronov](https://gitlab.com/serhii.voronov)

## 🫶 Show your support

Please ⭐️ this repository if this project helped you!

## 📝 License

Copyright © 2022 [Serhii Voronov](https://gitlab.com/serhii.voronov).<br />


---
