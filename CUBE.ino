#include <SPI.h>
#include "GyverHacks.h"     
#include <Wire.h>
#include <MPU6050.h>

//accelerometer and gyroscope variables
#define T_OUT 20
unsigned long int t_next;
MPU6050 accel;
int16_t ax_raw, ay_raw, az_raw, gx_raw, gy_raw, gz_raw;
int16_t ax_raw_prev = -1, ay_raw_prev = -1;
int16_t ax_raw_st, ay_raw_st, az_raw_st, gx_raw_st, gy_raw_st, gz_raw_st;

//axes
#define XAXIS 0
#define YAXIS 1
#define ZAXIS 2

//shift directions
#define POS_X 0
#define NEG_X 1
#define POS_Z 2
#define NEG_Z 3
#define POS_Y 4
#define NEG_Y 5

//capacity buttons
#define BUTTON1 6


#define TOTAL_EFFECTS 8

/*
   EFFECTS:
   - Light
   - Wave thin
   - Rain
   - Scace center
   - Scale corner
   - Random
   - Accelerometer drop
   - Accelerometer surface
*/

//  modes
#define LIT 0
#define WAVE_THIN 1 
#define RAIN 2
#define WOOP_WOOP 3
#define CUBE_JUMP 4
#define GLOW 5
#define ACCEL_DROP 6
#define ACCEL_SURF 7

#define RED_LED 14
#define GREEN_LED 15

// *** TIMERS *** 
#define RAIN_TIME 260
#define WOOP_WOOP_TIME 350
#define CUBE_JUMP_TIME 200
#define GLOW_TIME 8


uint8_t cube[8][8];
int8_t currentEffect;
uint16_t timer;
uint16_t modeTimer;
bool loading;
int8_t pos;

uint8_t btn1 = 0; //button1

void setup() {
  Serial.begin(115200);
  Serial.println("Initialize MPU6050");
  accel.initialize();
  SPI.begin();
  SPI.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  setPin(SS, LOW);
  SPI.transfer(0x01);

  //setting up the cube - nothing lighted
  for(int i = 0; i < 8; i++){
    SPI.transfer(0x00);    
  }
  setPin(SS, HIGH);

  // ***SET UP ACCELEROMETR **  
  accel.getMotion6(&ax_raw_st, &ay_raw_st, &az_raw_st, &gx_raw_st, &gy_raw_st, &gz_raw_st);

  //DEBUG:
  //char tbs[30];
  //sprintf(tbs, "x: %d y: %d", ax_raw_st, ay_raw_st);
  //Serial.println(tbs); 
   

  pinMode(BUTTON1,INPUT);     
  randomSeed(analogRead(0));

  loading = true;
  //starting the first mode
  currentEffect = LIT;
  changeMode();
}

uint8_t btn1_nw;

void loop() {
  btn1_nw = digitalRead(BUTTON1);  
  
  if (btn1_nw ) {  
    currentEffect++;
    if (currentEffect >= TOTAL_EFFECTS) currentEffect = 0;
    changeMode();
  }
  
  btn1 = btn1_nw;  
  
  //we choose effect every time because at one iteration only one layer is sent
  //so we need to come back to the same layer and continue
  
  switch (currentEffect) {
    case LIT:        lit();        break;
    case WAVE_THIN:  sinusThin();  break;
    case RAIN:       rain();       break;
    case WOOP_WOOP:  woopWoop();   break;
    case CUBE_JUMP:  cubeJump();   break;
    case GLOW:       glow();       break;
    case ACCEL_DROP: accel_drop(); break;
    case ACCEL_SURF: accel_surf(); break;
  }
  renderCube();
}

void changeMode() {
  clearCube();
  loading = true;
  timer = 0;
  randomSeed(millis());

  digitalWrite(RED_LED, HIGH);
  digitalWrite(GREEN_LED, LOW);
  delay(500);
  digitalWrite(RED_LED, LOW);
  digitalWrite(GREEN_LED, HIGH);

  switch (currentEffect) {
    case RAIN:      modeTimer = RAIN_TIME; break;
    case WOOP_WOOP: modeTimer = WOOP_WOOP_TIME; break;
    case CUBE_JUMP: modeTimer = CUBE_JUMP_TIME; break;
    case GLOW:      modeTimer = GLOW_TIME; break;
    case LIT:       modeTimer = RAIN_TIME; break;
    case WAVE_THIN: modeTimer = RAIN_TIME; break;
  }
}

/*
 * renderCube sends a layer by layer commands to light certain diods.
 * One layer is sent at one function call.
 * By repeatedly calling the function in loop we make an image seem holistic.
 */
void renderCube() {
for (uint8_t i = 0; i < 8; i++) {
  
    setPin(SS, LOW);
    SPI.transfer(0x01 << i);
    
    for (uint8_t j = 0; j < 8; j++) {
      SPI.transfer(cube[i][j]);
    }
    setPin(SS, HIGH);

    //provides dynamic lighting in combination with loop 
    delayMicroseconds(50);
  }

  // stop transfering
  setPin(SS, LOW);
  SPI.transfer(0x00);
  setPin(SS, HIGH);
}



//  *** WAVE MODE ***

void sinusThin() {
  if (loading) {
    clearCube();
    loading = false;
  }
  timer++;
  if (timer > modeTimer) {
    timer = 0;
    clearCube();
    if (++pos > 10) pos = 0;  
    for (uint8_t i = 0; i < 8; i++) {
      for (uint8_t j = 0; j < 8; j++) {
        //formula got by experimenting with different values from 2 to 4 dependent on sin(i) and pos
        int8_t sinZ = 4 + ((float)sin((float)(i + pos) / 2) * 2);  
        setVoxel(i, sinZ, j);
      }
    }
  }
}

//  *** RAIN MODE ***

void rain() {
  if (loading) {
    clearCube();
    loading = false;
  }
  timer++;
  if (timer > modeTimer) {
    timer = 0;
    shift(NEG_Y);
    uint8_t numDrops = random(0, 5);
    for (uint8_t i = 0; i < numDrops; i++) {
      setVoxel(random(0, 8), 7, random(0, 8)); // X Z Y
    }
  }
}

//  *** Accelerometer drop MODE ***
 
uint8_t c11 = 3, c21 = 4, c31 = 3, c41 = 4;
uint8_t c12 = 3, c22 = 3, c32 = 4, c42 = 4;
uint8_t drop_position = 0;
void accel_drop() {
  if (loading) {
    clearCube();
    loading = false;
    setVoxel(c11, 7, c12);
    setVoxel(c21, 7, c22);
    setVoxel(c31, 7, c32);
    setVoxel(c41, 7, c42);
    setVoxel(c11, 6, c12);
    setVoxel(c21, 6, c22);
    setVoxel(c31, 6, c32);
   setVoxel(c41, 6, c42);
   drop_position = 0;
  }

    
  timer++;
  if (timer > 150) {
    
        accel.getMotion6(&ax_raw, &ay_raw, &az_raw, &gx_raw, &gy_raw, &gz_raw);
       //DEBUG: 
       /* char tbs[30];
        sprintf(tbs, "x: %d y: %d", az_raw_st, az_raw);
        Serial.println(tbs); */
    
    timer = 0;
    if(az_raw_st - az_raw > 100 && drop_position > 0){
      shift(POS_Y);
      drop_position--;
    }
    if(az_raw_st - az_raw < - 100 && drop_position < 6){
      shift(NEG_Y);
      shift(NEG_Y);
      drop_position+=2;
    }
    
    if(ax_raw_st - ax_raw > 500 && c11 > 0){
      c11--; c21--; c31--; c41--;
      shift(NEG_X);
    }
    if(ax_raw_st - ax_raw < -500 && c21 < 7){
      c11++; c21++; c31++; c41++;
      shift(POS_X);
    }
    if(ay_raw_st - ay_raw > 500 && c12 > 0){
      c12--; c22--; c32--; c42--;
      shift(NEG_Z);
    }
    if(ay_raw_st - ay_raw < -500 && c42 < 7){
      c12++; c22++; c32++; c42++;
      shift(POS_Z);
    }
  }
}

// *** ACELEROMETR PLATFORM *** 
uint8_t iteration = 1;
uint8_t cube_surface[8][8] = {
                        {3,3,3,3,3,3,3,3},
                        {3,3,3,3,3,3,3,3},
                        {3,3,3,3,3,3,3,3},
                        {3,3,3,3,3,3,3,3},
                        {3,3,3,3,3,3,3,3},
                        {3,3,3,3,3,3,3,3},
                        {3,3,3,3,3,3,3,3},
                        {3,3,3,3,3,3,3,3}
};
int prev_x_diff = 0, prev_y_diff = 0;
void accel_surf() {

  if (loading) {
    timer = 0;
    clearCube();
    loading = false;
    
    for(int i = 0; i < 8; i++){
      for(int j = 0; j < 8; j++){
        cube_surface[i][j] = 3;
        setVoxel(j,cube_surface[i][j],i);  
      }
    }
    prev_x_diff = 0;
    prev_y_diff = 0;
  }
  
  timer++;
  if (timer > 50) {
        timer = 0;
        accel.getMotion6(&ax_raw, &ay_raw, &az_raw, &gx_raw, &gy_raw, &gz_raw);
       
        
        if(prev_y_diff - ay_raw < -1000){
            uint8_t num_lines;
            if(ay_raw - ay_raw_st > 0)
              num_lines = 1;
             else
              num_lines = 4;
            for(int diff = abs(ay_raw - ay_raw_st); diff > 0; diff -= 1000){
              for(uint8_t i = 0; i < min(4,num_lines); i++){
                for(uint8_t j = 0; j < 8; j++){
                  if(cube_surface[i][j] > i) {
                    clearVoxel(j,cube_surface[i][j],i);
                    cube_surface[i][j] -= 1;
                    setVoxel(j,cube_surface[i][j],i);
                  }
                }
              }

              for(uint8_t i = 7; i > max(3,8 - num_lines + 1); i--){
                for(uint8_t j = 0; j < 8; j++){
                  if(cube_surface[i][j] < i) {
                    clearVoxel(j,cube_surface[i][j],i);
                    cube_surface[i][j] += 1;
                    setVoxel(j,cube_surface[i][j],i);
                  }
                }
              }
              
             if(ay_raw - ay_raw_st > 0)
                  num_lines++;
                else
                  num_lines--;
            }
            prev_y_diff = ay_raw;
    }

    if(prev_y_diff  - ay_raw > 1000){
            uint8_t num_lines;
            if(ay_raw_st - ay_raw > 0)
              num_lines = 1;
             else
              num_lines = 4;
            for(int diff = abs(ay_raw_st - ay_raw); diff > 0; diff -= 1000){
              for(uint8_t i = 0; i < min(4,num_lines); i++){
                for(uint8_t j = 0; j < 8; j++){
                  if(cube_surface[i][j] < 7 - i) { 
                    clearVoxel(j,cube_surface[i][j],i);
                    cube_surface[i][j] += 1;
                    setVoxel(j,cube_surface[i][j],i);
                  }
                }
              }

              for(uint8_t i = 7; i > max(3,8 - num_lines); i--){
                for(uint8_t j = 0; j < 8; j++){
                  if(cube_surface[i][j] > 7 - i) {
                    
                    clearVoxel(j,cube_surface[i][j],i);
                    cube_surface[i][j] -= 1;
                    setVoxel(j,cube_surface[i][j],i);
                  }
                }
              }
                if(ay_raw_st - ay_raw > 0)
                  num_lines++;
                else
                  num_lines--;
            }
            prev_y_diff = ay_raw;
    }
    
   if(prev_x_diff  - ax_raw > 1000){
            uint8_t num_lines;
            if(ax_raw_st - ax_raw > 0)
              num_lines = 1;
             else
              num_lines = 4;
            for(int diff = abs(ax_raw_st - ax_raw); diff > 0; diff -= 1000){
              for(uint8_t i = 0; i < min(4,num_lines); i++){
                for(uint8_t j = 0; j < 8; j++){
                  if(cube_surface[j][i] < 7 - i) { 
                    clearVoxel(i,cube_surface[j][i],j);
                    cube_surface[j][i] += 1;
                    setVoxel(i,cube_surface[j][i],j);
                  }
                }
              }

              for(uint8_t i = 7; i > max(3,8 - num_lines); i--){
                for(uint8_t j = 0; j < 8; j++){
                  if(cube_surface[j][i] > 7 - i) {
                    
                    clearVoxel(i,cube_surface[j][i],j);
                    cube_surface[j][i] -= 1;
                    setVoxel(i,cube_surface[j][i],j);
                  }
                }
              }
                if(ax_raw_st - ax_raw > 0)
                  num_lines++;
                else
                  num_lines--;
            }
            prev_x_diff = ax_raw;
    }

    if(prev_x_diff - ax_raw < -1000){
            uint8_t num_lines;
            if(ax_raw - ax_raw_st > 0)
              num_lines = 1;
             else
              num_lines = 4;
            for(int diff = abs(ax_raw - ax_raw_st); diff > 0; diff -= 1000){
              for(uint8_t i = 0; i < min(4,num_lines); i++){
                for(uint8_t j = 0; j < 8; j++){
                  if(cube_surface[j][i] > i) {
                    clearVoxel(i,cube_surface[j][i],j);
                    cube_surface[j][i] -= 1;
                    setVoxel(i,cube_surface[j][i],j);
                  }
                }
              }

              for(uint8_t i = 7; i > max(3,8 - num_lines + 1); i--){
                for(uint8_t j = 0; j < 8; j++){
                  if(cube_surface[j][i] < i) {
                    clearVoxel(i,cube_surface[j][i],j);
                    cube_surface[j][i] += 1;
                    setVoxel(i,cube_surface[j][i],j);
                  }
                }
              }
              
             if(ax_raw - ax_raw_st > 0)
                  num_lines++;
                else
                  num_lines--;
            }
            prev_x_diff = ax_raw;
    }
      
  }
}

// *** SCALING CUBE CENTER ***
uint8_t cubeSize = 0;
bool cubeExpanding = true;
void woopWoop() {
  if (loading) {
    clearCube();
    cubeSize = 2;
    cubeExpanding = true;
    loading = false;
  }

  timer++;
  if (timer > modeTimer) {
    timer = 0;
    if (cubeExpanding) {
      cubeSize += 2;
      //if cube has the biggest size possible
      if (cubeSize == 8) 
        cubeExpanding = false;
    } 
    else {
      cubeSize -= 2;
      //if cube reached the smallest size
      if (cubeSize == 2) 
        cubeExpanding = true;
    }
    clearCube();
    drawCube(4 - cubeSize / 2, 4 - cubeSize / 2, 4 - cubeSize / 2, cubeSize);
  }
}

// *** SCALING CUBE CORNERS ***
uint8_t xPos;
uint8_t yPos;
uint8_t zPos;
void cubeJump() {
  if (loading) {
    clearCube();
    //randomly seceting the start position
    //random(0,2) - select start or end of the axis
    //*7 get the coordinate of the voxel (0,7)
    xPos = random(0, 2) * 7;
    yPos = random(0, 2) * 7;
    zPos = random(0, 2) * 7;
    cubeSize = 8;
    cubeExpanding = false;
    loading = false;
  }

  timer++;
  if (timer > modeTimer) {
    //start again
    timer = 0;
    clearCube();
    if (xPos == 0 && yPos == 0 && zPos == 0) {
      drawCube(xPos, yPos, zPos, cubeSize);
    } else if (xPos == 7 && yPos == 7 && zPos == 7) {
      drawCube(xPos + 1 - cubeSize, yPos + 1 - cubeSize, zPos + 1 - cubeSize, cubeSize);
    } else if (xPos == 7 && yPos == 0 && zPos == 0) {
      drawCube(xPos + 1 - cubeSize, yPos, zPos, cubeSize);
    } else if (xPos == 0 && yPos == 7 && zPos == 0) {
      drawCube(xPos, yPos + 1 - cubeSize, zPos, cubeSize);
    } else if (xPos == 0 && yPos == 0 && zPos == 7) {
      drawCube(xPos, yPos, zPos + 1 - cubeSize, cubeSize);
    } else if (xPos == 7 && yPos == 7 && zPos == 0) {
      drawCube(xPos + 1 - cubeSize, yPos + 1 - cubeSize, zPos, cubeSize);
    } else if (xPos == 0 && yPos == 7 && zPos == 7) {
      drawCube(xPos, yPos + 1 - cubeSize, zPos + 1 - cubeSize, cubeSize);
    } else if (xPos == 7 && yPos == 0 && zPos == 7) {
      drawCube(xPos + 1 - cubeSize, yPos, zPos + 1 - cubeSize, cubeSize);
    }
    if (cubeExpanding) {
      cubeSize++;
      if (cubeSize == 8) {
        cubeExpanding = false;
        xPos = random(0, 2) * 7;
        yPos = random(0, 2) * 7;
        zPos = random(0, 2) * 7;
      }
    } else {
      cubeSize--;
      if (cubeSize == 1) {
        cubeExpanding = true;
      }
    }
  }
}


// *** RANDOM FILL ***
bool glowing;
uint16_t glowCount = 0;
uint8_t selX = 0;
uint8_t selY = 0;
uint8_t selZ = 0;
void glow() {
  if (loading) {
    clearCube();
    glowCount = 0;
    glowing = true;
    loading = false;
  }

  timer++;
  if (timer > modeTimer) {
    timer = 0;
    if (glowing) {
      if (glowCount < 448) { 
        //when it is still possible to see separate lighted voxel
        do {
          selX = random(0, 8);
          selY = random(0, 8);
          selZ = random(0, 8);
        } while (getVoxel(selX, selY, selZ));
        setVoxel(selX, selY, selZ);
        glowCount++;
      } else if (glowCount < 512) {
        //it is no visible difference between lighting whole cube and new voxel now
        //but I want to keep the time of lighting each voxel
        lightCube();
        glowCount++;
      } else {
        //start again
        glowing = false;
        glowCount = 0;
      }
    } else {
      if (glowCount < 448) {
        do {
          //get random new voxel and check if not lit already
          selX = random(0, 8);
          selY = random(0, 8);
          selZ = random(0, 8);
        } while (!getVoxel(selX, selY, selZ));
        clearVoxel(selX, selY, selZ);
        glowCount++;
      } else {
        clearCube();
        glowing = true;
        glowCount = 0;
      }
    }
  }
}




// *** FULL LIGHT ***
void lit() {
  if (loading) {
    clearCube();
    lightCube();
    loading = false;
  }
}

void setVoxel(uint8_t x, uint8_t y, uint8_t z) {
  cube[7 - y][7 - z] |= (0x01 << x);
}

void clearVoxel(uint8_t x, uint8_t y, uint8_t z) {
 
  cube[7 - y][7 - z] ^= (0x01 << x);
}

bool getVoxel(uint8_t x, uint8_t y, uint8_t z) {
  return (cube[7 - y][7 - z] & (0x01 << x)) == (0x01 << x);
}


//shifts the image to the set direction
void shift(uint8_t dir) {

  if (dir == POS_X) {
    for (uint8_t y = 0; y < 8; y++) {
      for (uint8_t z = 0; z < 8; z++) {
        cube[y][z] = cube[y][z] << 1;
      }
    }
  } else if (dir == NEG_X) {
    for (uint8_t y = 0; y < 8; y++) {
      for (uint8_t z = 0; z < 8; z++) {
        cube[y][z] = cube[y][z] >> 1;
      }
    }
  } else if (dir == POS_Y) {
    for (uint8_t y = 1; y < 8; y++) {
      for (uint8_t z = 0; z < 8; z++) {
        cube[y - 1][z] = cube[y][z];
      }
    }
    for (uint8_t i = 0; i < 8; i++) {
      cube[7][i] = 0;
    }
  } else if (dir == NEG_Y) {
    for (uint8_t y = 7; y > 0; y--) {
      for (uint8_t z = 0; z < 8; z++) {
        cube[y][z] = cube[y - 1][z];
      }
    }
    for (uint8_t i = 0; i < 8; i++) {
      cube[0][i] = 0;
    }
  } else if (dir == POS_Z) {
    for (uint8_t y = 0; y < 8; y++) {
      for (uint8_t z = 1; z < 8; z++) {
        cube[y][z - 1] = cube[y][z];
      }
    }
    for (uint8_t i = 0; i < 8; i++) {
      cube[i][7] = 0;
    }
  } else if (dir == NEG_Z) {
    for (uint8_t y = 0; y < 8; y++) {
      for (uint8_t z = 7; z > 0; z--) {
        cube[y][z] = cube[y][z - 1];
      }
    }
    for (uint8_t i = 0; i < 8; i++) {
      cube[i][0] = 0;
    }
  }
}

void drawCube(uint8_t x, uint8_t y, uint8_t z, uint8_t s) {
  for (uint8_t i = 0; i < s; i++) { // s - in fakt its a lenhgth of side of cube
    //draw in all 4 directions
    setVoxel(x, y + i, z);
    setVoxel(x + i, y, z);
    setVoxel(x, y, z + i);
    
    setVoxel(x + s - 1, y + i, z + s - 1);
    setVoxel(x + i, y + s - 1, z + s - 1);
    setVoxel(x + s - 1, y + s - 1, z + i);
    
    setVoxel(x + s - 1, y + i, z);
    setVoxel(x, y + i, z + s - 1);
    setVoxel(x + i, y + s - 1, z);
    
    setVoxel(x + i, y, z + s - 1);
    setVoxel(x + s - 1, y, z + i);
    setVoxel(x, y + s - 1, z + i);
  }
}

//sets all the voxels to max light
void lightCube() {
  for (uint8_t i = 0; i < 8; i++) {
    for (uint8_t j = 0; j < 8; j++) {
      cube[i][j] = 0xFF;
    }
  }
}

//sets all the voxels to 0
void clearCube() {
  for (uint8_t i = 0; i < 8; i++) {
    for (uint8_t j = 0; j < 8; j++) {
      cube[i][j] = 0;
    }
  }
}
